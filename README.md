The Costa Rican Social Security Fund or Caja Costarricense de Seguro Social (as it is known in Spanish) is in charge of most of the nation's public health sector. Its role in public health (as the administrator of health institutions) is key in Costa Rica, playing an important part in the state's national health policy making.

Its services are available to all citizens and permanent legal residents. This governmental entity's functions encompass both the administrative and functional aspects. Originally, services were carried out at private hospitals but funded by the Costa Rican Social Security Fund. Not until the mid-1960s did the Caja begin constructing its own hospitals staffed by public employees.[2] It has the obligation (as a public institution) to formulate and execute health programs that are both preventive (such as: vaccination, informational, fumigation, etc.) and healing (such as: surgery, radiation therapy, pharmacy, clinical, etc.) in nature.[3]

The Costa Rican Social Security Fund is also charged with the administration of the public pension system.

##Internal Organization

The Costa Rican Social Security Fund has a complex internal organization which administers and oversees all of the entity's actions in every aspect of its functions. It is strategically managed and administered by the Board of Directors, Executive Presidency, and six Managements. Additionally, it has a supervising body that oversees the administrative actions taken.[1]

###Executive Presidency

The current Executive President is doctor Román Macaya.[4] The Executive President also chairs the Board of Directors and is appointed by the government.

##Funding

The federal government is legally required to assume any and all debt that the Caja amasses. The Caja budget had almost septupled from 1972-1979. By 1982, following economic crisis, the program faced a budget deficit of $26 million, equal to a third of its annual budget. Originally, Caja employees received coverage despite not having a payroll tax for the program. This was ended in 1982, when a 4% payroll tax was instituted. Likewise the payroll tax share paid by non-Caja employers was increased from 6.75% to 9.25%.[5] As of 2006, 76% of the Caja budget came from employee and employer contributions. Services for low-income residents who have their coverage paid by the state are funded by a luxury goods tax.[6] From 1982-1985, the Social Security Fund constructed 150 new rural health posts and increased training of medical workers, despite International Monetary Fund austerity measures being in effect.[5] In 1995, 36% of public social spending, itself 13% of GDP, went toward health care.[2]

##Hospitals and clinics

Hospitals and clinics under CCSS administration:

###Alajuela

1. Hospital San Carlos
2. Hospital San Rafael de Alajuela
3. Hospital Carlos Luis Valverde Vega (San Ramón)
4. Hospital Los Chiles
5. Hospital Upala
6. Hospital San Francisco de Asis (Grecia)


###Cartago

1. Hospital Max Peralta

###Guanacaste

1. Hospital La Anexión

###Heredia

1. Hospital San Vicente de Paúl (Heredia)

###Limón

1. Hospital Dr. Tony Facio Castro

###Puntarenas

1. Hospital de Golfito
2. Hospital Monseñor Víctor Manuel Sanabria


###Clínica de Miramar

1. Hospital de Ciudad Nelly
2. Hospital de San Vito
3. Max Teran Vals Quepos


###San José

1. Hospital Nacional de Niños Carlos Sáenz Herrera, in the city centre
2. Hospital San Juan de Dios, in the city centre
3. Hospital Mexico, west of San José
4. Hospital Rafael Ángel Calderón Guardia, in El Carmen district
5. Hospital de la Mujer (Antigua Maternidad Adolfo Carit), in San Sebastián district
6. Clínica Doctor Carlos Durán Cartín, in Zapote District.
7. Clínica Doctor Ricardo Moreno Cañas
8. Clínica Marcial Fallas, in Desamparados canton
9. Clínica Doctor Solón Núñez
10. Hospital Dr. Fernando Escalante Pradilla, in Pérez Zeledón

1. 
The Executive President is the highest office any one person can hold within the organization's hierarchy and is charged with the responsibility of executing the Board of Directors' decision. Among other functions, responsibilities, and obligations, the Executive President must coordinate with other government institutions wherever possible or needed.

Apart from having state given obligations (which are exposed in the national law) the Board of Directors can assign other corresponding responsibilities. The Executive President is obliged to provide exclusive services to the Costa Rican Social Security Fund; meaning that he or she may not hold any other public office or work independently while serving as Executive President.
